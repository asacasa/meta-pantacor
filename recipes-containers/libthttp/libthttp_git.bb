SUMMARY = "tiny httpd from pantacor"
HOMEPAGE = "https://gitlab.com/pantacor/libthttp"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://LICENSE;md5=bd0a4fad56a916f12a1c3cedb3976612"

SRC_URI = "git://gitlab.com/pantacor/${PN}.git;protocol=https"
SRCREV = "${AUTOREV}"

EXTRA_OEMAKE += " BUILDDIR=${S} DESTDIR=${D} CONFIG_PREFIX=${D}/${sysconfdir}"
S = "${WORKDIR}/git"

do_install() {
	oe_runmake install
}

FILES_${PN} += "${sysconfdir}/certs/*"
