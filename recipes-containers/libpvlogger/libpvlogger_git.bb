SUMMARY = "Pantavisor logging"
HOMEPAGE = "https://gitlab.com/pantacor/pv-libpvlogger"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://logger.h;md5=fc441c9f90aaf2159dd1637968bc4c31"

inherit cmake
SRC_URI = "git://gitlab.com/pantacor/pv-${PN}.git;protocol=https \
	   file://0001-Add-cmake-for-pvlogger.patch \
	"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
CFLAGS_append = " -Wstringop-overflow=0 -Wformat-truncation=0 -Wformat-overflow=0"
