PREFERRED_PROVIDER_virtual/kernel = "linux-yocto-tiny"
PREFERRED_VERSION_linux-yocto = "5.0%"

SERIAL_CONSOLES = "115200;ttyS0"
FITLOADADDR = "0x80008000"
KERNEL_CLASSES += "kernel-fitimage"
KERNEL_IMAGETYPE = "fitImage"
INITRAMFS_IMAGE = "pv-image-initramfs"

IMAGE_BOOT_FILES = "fitImage-${INITRAMFS_IMAGE}-${MACHINE}.bin;fitImage"
EXTRA_IMAGEDEPENDS += "u-boot"
WKS_FILE_DEPENDS_BOOTLOADERS ?= "u-boot"
