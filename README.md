# meta-pantacor Image Build

## Required packages on Ubuntu

This section provides required packages on an Ubuntu Linux distribution:

### Essentials

Packages needed to build an image for a headless system:

```shell
$ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping python-git repo bmap-tools
```

## Quick step

```shell
$ repo init -u ssh://git@gitlab.com:parthiban.nallathambi/meta-pantacor.git -m conf/samples/beaglebone-yocto/pv-beaglebone.xml -b master ; repo sync -j10; cd beagle/; TEMPLATECONF=meta-pantacor/conf/samples/beaglebone-yocto source oe-init-build-env; bitbake pv-image-initramfs
```

## Download meta-pantacor project manifest

To easily manage different git repositories layers, meta-pantacor project is using [Android repo tool](https://source.android.com/source/using-repo),

First initialize repo specifying the project manifest and the corresponding branch:

```shell
$ repo init -u ssh://git@gitlab.com:parthiban.nallathambi/meta-pantacor.git -m conf/samples/beaglebone-yocto/pv-beaglebone.xml -b master
```

then checkout the project source tree:

```shell
$ repo sync -j10
```

## Configuring the project

meta-pantacor offers pre-configured machine templates, tested and ready to use.

- beaglebone black

```shell
$ cd beagle/
$ TEMPLATECONF=meta-pantacor/conf/samples/beaglebone-yocto source oe-init-build-env
```

## Build the project

### SD card image
```shell
$ bitbake pv-image-initramfs
```
